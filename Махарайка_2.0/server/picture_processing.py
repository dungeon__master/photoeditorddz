# os.path - модуль необходим для обработки существования папок в части кода работы со скроллерами
import os

# ImghDr - использую для проверки файла, на то является ли он изображением. Встроенная библиотека
import imghdr

# Python Imaging Library - открываем, обрабатываем и сохраняем из-е
from PIL import (
    Image,
    ImageEnhance,
)

# NumPy - содержит математические операции, с помощью которых мы переводим из-е в сепию
import numpy as np

# OpenCV - применяем обработку из-й
import cv2
import config


class EditIm:
    """Содержит функции перевода изображения в различные фильтры"""

    def black_white_image(self, image_name):
        """Применение фильтра "ЧБ" к изображению

        Args:
            image_name (string): имя изображения с расширением
        """
        # Считывание полного пути до изображения
        path = config.PATH_TO_IMAGES + image_name
        if not os.path.exists(path):
            return "File not exist"
        if imghdr.what(path) is None:
            return "Файл не является изображением"

        # Чтение изображения через cv2
        input_image = cv2.imread(path)
        # Перевод изображения в ЧБ
        output_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
        # Сохранение изображения по пути
        cv2.imwrite(config.PATH_TO_IMAGES + image_name, output_image)
        if not os.path.exists("tmp"):
            # Создание новой папки для оригинального изображения
            os.mkdir("tmp")
        cv2.imwrite(config.PATH_TO_TMP + image_name, output_image)
        return True

    def sepia_image(self, image_name):
        """Применение фильтра "сепия" к изображению

        Args:
            image_name (string): имя изображения с расширением
        """
        # Считывание полного пути до изображения
        path = config.PATH_TO_IMAGES + image_name
        if not os.path.exists(path):
            return "Отсутствует файл изображения"
        if not imghdr.what(path):
            return "Файл не является изображением"
        # Чтение изображения через cv2
        input_image = cv2.imread(path)

        # С помощью NumPy создадим массив из изображения и переведем его с помощью мат ф-й в сепию
        output_image = np.array(input_image, dtype=np.float64)
        output_image = cv2.transform(
            output_image,
            np.matrix(
                [[0.393, 0.769, 0.189], [0.349, 0.686, 0.168], [0.272, 0.534, 0.131]]
            ),
        )
        output_image[np.where(output_image > 255)] = 255
        # Возвращение изображения
        output_image = np.array(output_image, dtype=np.uint8)
        # Сохранение изображения по пути
        cv2.imwrite(config.PATH_TO_IMAGES + image_name, output_image)
        if not os.path.exists("tmp"):
            # Создание новой папки для оригинального изображения
            os.mkdir("tmp")
        cv2.imwrite(config.PATH_TO_TMP + image_name, output_image)
        return True


    def invert_image(self, image_name):
        """Применение фильтра "негатив" к изображению

        Args:
            image_name (string): имя изображения с расширением
        """
        # Считывание полного пути до изображения
        path = config.PATH_TO_IMAGES + image_name
        if not os.path.exists(path):
            return "Отсутствует файл изображения"
        if not imghdr.what(path):
            return "Файл не является изображением"

        # Чтение изображения через cv2
        input_image = cv2.imread(path)

        # Перевод изображения в негатив
        output_image = cv2.bitwise_not(input_image)

        # Сохранение изображения по пути
        cv2.imwrite(config.PATH_TO_IMAGES + image_name, output_image)
        if not os.path.exists("tmp"):
            # Создание новой папки для оригинального изображения
            os.mkdir("tmp")
        cv2.imwrite(config.PATH_TO_TMP + image_name, output_image)
        return True

    def contrast_sharpness_image(self, image_name, contr_coef, sharp_coef):
        """Применение фильтра контраста и/или резкости к изображению

        Args:
            image_name (string): имя изображение с расширением
            contr_coef (int): степень контраста изображения
            sharp_coef (int): степень резкости изображения
            (1 - исходное из-е, >1 - увеличенный контраст, <1 - уменьшенный контраст)
        """

        # Проверка существования временной папки с оригиналом изображения
        if not os.path.exists("tmp"):
            # Создание новой папки для оригинального изображения
            os.mkdir("tmp")
            # Создание пути до изображения
            orig_image = config.PATH_TO_IMAGES + image_name
            # Чтение изображения через PIL
            input_image = Image.open(orig_image)
            # Сохранение изображения во временную папку
            input_image.save("./tmp/" + image_name)

        # Создание пути до изображения
        orig_image = "./tmp/" + image_name
        if not os.path.exists(orig_image):
            return "Отсутствует файл изображения"
        if not imghdr.what(orig_image):
            return "Файл не является изображением"
        # Чтение изображения через PIL
        input_image = Image.open(orig_image)
        # Создание усилителя для из-я по контрасту
        enhancer = ImageEnhance.Contrast(input_image)
        # Применение коэфф-та к усилителю
        output_image = enhancer.enhance(contr_coef)
        # Создание усилителя для из-я по резкости
        enhancer = ImageEnhance.Sharpness(output_image)
        # Применение коэфф-та к усилителю
        output_image = enhancer.enhance(sharp_coef)
        # Сохранение изображения по пути
        output_image.save(config.PATH_TO_IMAGES + image_name)
        return True
