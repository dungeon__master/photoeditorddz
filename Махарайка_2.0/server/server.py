# os, shutil - модуль, примененные для работы с системными компонентами
import os
import shutil

# socket, threading - модули, использованные для реализации клиент-серверной архитектуры
import socket
import threading

# marshal - модуль, примененный для обмена данными между клиентом и сервером
import marshal

import config
from picture_processing import EditIm
from user_manager import UserManager
from photo_manager import PhotoManager

LOCALHOST = "127.0.0.1"
PORT = 1356


class ClientThread(threading.Thread):
    """Обработка подключившегося клиента"""

    def __init__(self, client_address, clientsocket):
        threading.Thread.__init__(self)
        self.csocket = clientsocket
        print("Новое подключение", client_address)

    def run(self):
        """Запускает клиентский поток"""
        message = ""
        while True:
            # Размер чанка
            data = self.csocket.recv(3072000)

            # Декодируем полученные байты
            if data is not None:
                message = marshal.loads(data)
                print(message)

            # Обрабатываем различные события
            if message[0] == "login":
                login = message[1]
                password = message[2]
                # Получаем результат выполнения функции
                func_res = auth(login, password)
                # Переводим данные в бинарный вид
                serialize_data = marshal.dumps(func_res)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "add":
                # Получаем результат выполнения функции
                func_res = PhotoManager().add_image(message[1], message[2])
                # Переводим данные в бинарный вид
                serialize_data = marshal.dumps(func_res)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "update":
                # Получаем результат выполнения функции
                func_res = PhotoManager().load_data(message[1])
                # Переводим данные в бинарный вид
                serialize_data = marshal.dumps(func_res)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "edit":
                # Получаем результат выполнения функции
                func_res = save_image_in_dir(message[1], message[2], message[3])
                # Переводим данные в бинарный вид
                serialize_data = marshal.dumps(func_res)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "b&w":
                # Применяем фильтр к изображению
                EditIm().black_white_image(message[1])
                # Переводим изображение в бинарный вид
                image = PhotoManager().import_pict_binary(
                    config.PATH_TO_IMAGES + message[1]
                )
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "sepia":
                # Применяем фильтр к изображению
                EditIm().sepia_image(message[1])
                # Переводим изображение в бинарный вид
                image = PhotoManager().import_pict_binary(
                    config.PATH_TO_IMAGES + message[1]
                )
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "negative":
                # Применяем фильтр к изображению
                EditIm().invert_image(message[1])
                # Переводим изображение в бинарный вид
                image = PhotoManager().import_pict_binary(
                    config.PATH_TO_IMAGES + message[1]
                )
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "contrast_sharpness":
                # Применяем фильтр к изображению
                EditIm().contrast_sharpness_image(
                    message[1], int(message[2]), int(message[3])
                )
                # Переводим изображение в бинарный вид
                image = PhotoManager().import_pict_binary(
                    config.PATH_TO_IMAGES + message[1]
                )
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "clean":
                # Очищаем папки "tmp" и "img"
                event_close(message[1])

            elif message[0] == "delete":
                # Удаляем изображение из БД
                PhotoManager().delete_image(message[1], message[2])

            elif message[0] == "save":
                # Получаем картинку для последующей выгрузки
                image = PhotoManager().take_image(message[1], message[2])
                # Сериализуем данные
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "cancel":
                # Получаем исходное изображение
                image = cancel(message[1], message[2], message[3])
                # Сериализуем данные
                serialize_data = marshal.dumps(image)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)

            elif message[0] == "save_change":
                error = save_changed_image(message[1], message[2])
                # Сериализуем данные
                serialize_data = marshal.dumps(error)
                # Отправка данных клиенту
                self.csocket.send(serialize_data)


def event_close(path):
    """Очищает папки при закрытии окна"""
    image_path = os.path.join(config.BASE_DIR, path)
    tmp_path = os.path.join(config.BASE_DIR, config.PATH_TO_TMP)
    try:
        # Удаление файла в папке img
        os.remove(image_path)
        # Удаление папки tmp
        shutil.rmtree(tmp_path)
    except:
        return False


def save_image_in_dir(login, image_name, extension):
    """Сохранение изображения в папку img"""
    # Формирование запроса
    PhotoManager().save_image(login, image_name, config.PATH_TO_IMAGES)
    image = PhotoManager().import_pict_binary(
        config.PATH_TO_IMAGES + image_name + extension
    )
    return image


def cancel(login, image_name, extension):
    """Обновляет изображения в папках img и tmp

    :param login: имя пользователя
    :param image_name: имя изображения
    :param extension: расширение изображения
    :return: изображение в BLOB
    """
    PhotoManager().save_image(login, image_name, config.PATH_TO_IMAGES)
    PhotoManager().save_image(login, image_name, config.PATH_TO_TMP)
    image = PhotoManager().import_pict_binary(
        config.PATH_TO_IMAGES + image_name + extension
    )
    return image


def save_changed_image(message, login):
    """Сохраняет измененное изображение в БД

    :param message: данные, отправляемые сервером
    :param login: имя пользователем
    :return: флаг ошибки
    """
    flag = PhotoManager().check_exist_image(message[0])
    # Загрузка изображения в БД. Если не существует - загружается новое,
    # если существует - обновляется старое
    if flag is True:
        PhotoManager().add_image(message, login)
        return "True"

    PhotoManager().update_image(message[0], message[1])
    return "True"


def auth(login, password):
    """Авторизирует пользователя

    :param login: имя пользователя
    :param password: пароль, введенный пользователем
    :return: флаг ошибки
    """
    flag = UserManager().check_validity(login, password)
    if flag == 2:
        return "False"

    # Открытие окна со списком фотографий при удачной аутентификации
    if flag == 0:
        return "True"
    if flag == 1:
        UserManager().reg_user(login, password)
        return "True"


# Настрой сокетов
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server.bind((LOCALHOST, PORT))
print("Сервер запущен!")

while True:
    # Прием клиентского подключениея и создание клиентского потока
    server.listen(1)
    clientsock, clientAddress = server.accept()
    newthread = ClientThread(clientAddress, clientsock)
    newthread.start()
