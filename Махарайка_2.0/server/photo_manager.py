# sqlite3 - используется для работы с БД. Встроенная библиотека
import sqlite3

# os - Используется для определения пути для сохрения или загрузки из-я
import os

# datetime - Используется для определения времени создания и редактирования из-я
from datetime import datetime

# pytz - Используетс для установки часового пояса
import pytz

from user_manager import UserManager
import config


class Database:
    """Взаимодействует с БД"""

    # Установка корректного пути к базу данных
    dbPath = os.path.join(config.BASE_DIR, config.PATH_TO_DB)

    def __init__(self):
        self.connect = None
        self.cursor  = None

    def connect_database(self):
        """Подключение к БД"""
        self.connect = sqlite3.connect(self.dbPath)
        self.cursor  = self.connect.cursor()

    def close(self):
        """Разрыв соединения с БД"""
        self.connect.close()


class PhotoManager(Database):
    """Взаимодействует с изображениями

    Args:
        Database (class): база данных
    """

    def import_pict_binary(self, image_path):
        """Переводит изображения в BLOB

        Args:
            image_path (string): путь к изображению

        Returns:
            BLOB: изображение в формате BLOB
        """

        # Взаимодействуем с изображением побайтно
        with open(image_path, "rb") as file:
            pict_binary = file.read()

        # Закрываем поток
        file.close()

        # Возвращение бинарника изображения
        return pict_binary

    def write_to_file(self, data, file_path):
        """Запись из БД по заданному пути

        Args:
            data (BLOB): бинарник изображения
            file_path (string): путь для записи изображения
        """

        with open(file_path, "wb") as file:
            file.write(data)

    def get_size(self, size):
        """Получает размер изображения

        Args:
            size (int): Размер в байтах

        Returns:
            str: Строка с информацией о размере изображения
        """

        # Рассчет размера изображение и вывод в его оптимальной мере размера
        # Изначально - размер файла в байтах
        full_size = str(size) + "Bytes"

        # Если размер > 2^10, то повысим степень и переведем изображение в килобайты
        if size > 1024:
            size = round(size / 1024 * 10) / 10
            full_size = str(size) + "Kb"

        # Если все ещё размер > 2^10, то повысим степень и переведем изображение в мегабайты
        if size > 1024:
            size = round(size / 1024 * 10) / 10
            full_size = str(size) + "Mb"
        return full_size

    def save_image(self, login, image_name, path=""):
        """Сохраняет изображение по пути

        Args:
            login (string): имя пользователя
            image_name (string): Название изображения
            path (str, optional): путь к месту сохранения изображения. по умолчанию ''.

        Returns:
            mixed: true - ошибок нет, иначе - ошибка
        """

        # Подключение к БД
        self.connect_database()

        # Формирование запроса
        uid = UserManager().get_user_id(login)
        request_to_picture = (
            "SELECT image, extension FROM picture WHERE name = ? and userId = ?"
        )
        image, extension = self.cursor.execute(
            request_to_picture, (image_name, uid)
        ).fetchone()

        # Если передан - сохраняем по пути
        image_path = path + "/" + image_name + extension

        # Запись изображения в файловую систему
        self.write_to_file(image, image_path)
        return True

    def delete_image(self, login, image_name):
        """Удаление изображения из БД

        Args:
            login (string): имя пользователя
            image_name (string): название изображения (без расширения)

        Returns:
            mixed: true - ошибок нет, иначе - ошибка
        """

        # Формирование запроса
        self.connect_database()

        # Получаем id пользователя
        uid = UserManager().get_user_id(login)

        # Запрос на удаление
        request = "DELETE from picture where name = ? and userID = ?"
        order = (image_name, uid)
        self.cursor.execute(request, order)
        self.connect.commit()

        # Разрыв соединения
        self.close()
        return True

    def add_image(self, meta_data, login):
        """Загрузка нового изображения в БД"""
        # Создание подключения к БД
        self.connect_database()

        # Формирование запроса
        uid = UserManager().get_user_id(login)
        request = "INSERT INTO picture(name, extension, createdAt, changedAt, size, image, userID)"\
                  "VALUES (?, ?, ?, ?, ?, ?, ?)"
        order = (
            meta_data[0],
            meta_data[1],
            meta_data[2],
            meta_data[3],
            meta_data[4],
            meta_data[5],
            uid,
        )
        try:
            self.cursor.execute(request, order)
            self.connect.commit()
            self.close()
            return "True"
        # Если изображение уже существует, то возвращаем ошибку
        except sqlite3.Error:
            self.close()
            return "Изображение с таким именем уже существует"

    def update_image(self, name, extension):
        """Занесение измененного изображения в БД

        Args:
            name (string): имя изображения
            extension (string): расширение изображения
        """

        # Установление подключения к БД
        self.connect_database()

        # Путь к изображению
        image = config.PATH_TO_IMAGES + name + extension
        image_path = os.path.join(config.BASE_DIR, image)

        # Возвращает бинарник изображения
        image = self.import_pict_binary(image_path)

        # Расчет размера изображения
        size = self.get_size(os.path.getsize(image_path))

        # Установление времени изменения изображения
        timezone = pytz.timezone("Europe/Moscow")
        date = datetime.now(timezone)

        # Убираем милисекунды
        time = str(date).split(".", maxsplit=1)[0]

        # Формирование запроса
        request = "UPDATE picture set changedAt = ?, size = ?, image = ? where name = ?"
        order = (time, size, image, name)
        self.cursor.execute(request, order)
        self.connect.commit()
        self.close()
        return True

    def check_exist_image(self, image_name):
        """Проверяет, есть ли изображение с таким именем в БД

        Args:
            image_name (string): имя изображения

        Returns:
            mixed: обработка ошибок
        """

        # Устанавливает соединение с БД
        self.connect_database()

        # Формирование запроса
        request = "SELECT pictureId from picture where name = ?"
        order = (image_name,)
        picture_id = self.cursor.execute(request, order).fetchone()
        self.close()

        # Если картинки нет - true, если есть - false
        if picture_id is None:
            return True
        return False

    def load_data(self, login):
        """Получение метаданных изображений из БД"""
        # Формирование подключения к БД
        self.connect_database()

        # Формирование запроса
        uid = UserManager().get_user_id(login)
        sql_query = "SELECT name, extension, createdAt, changedAt, size FROM picture"\
                    " WHERE userId = ?"
        info = self.cursor.execute(sql_query, (uid,)).fetchall()

        # Разрыв соединения
        self.close()
        return info

    def take_image(self, login, image_name):
        """Достает изображение из БД"""
        self.connect_database()

        uid = UserManager().get_user_id(login)
        request_to_picture = "SELECT image FROM picture WHERE name = ? and userId = ?"
        order = (image_name, uid)
        image = self.cursor.execute(request_to_picture, order).fetchone()
        self.connect.commit()
        self.close()
        return image[0]
