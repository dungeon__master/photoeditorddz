# sqlite3 - модуль, используемый для работы с БД
import sqlite3

# hashlib - модуль, используемый для создания хэша для шифрования пароля
import hashlib

# os.path - модуль, примененные для работы с системными компонентами
import os.path

# config - модуль, в котором прописаны константы (пути)
import config


class Database:
    """Взаимодействует с БД"""

    # Установка корректного пути к базу данных
    dbPath = os.path.join(config.BASE_DIR, config.PATH_TO_DB)

    def __init__(self):
        self.connect = None
        self.cursor  = None

    def connect_database(self):
        """Подключение к БД"""
        self.connect = sqlite3.connect(self.dbPath)
        self.cursor  = self.connect.cursor()

    def close(self):
        """Разрыв соединения с БД"""
        self.connect.close()


class UserManager(Database):
    """Взаимодействие с данными пользователя

    Args:
        Database (class): база данных
    """

    def get_user_data(self, login):
        """Достает информацию о пользователе

        Args:
            login (string): Имя пользователя

        Returns:
            [array]: Массив с userID, userName, password
        """

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Достаем из таблицы информацию о всех пользователях
        request = "SELECT userID, username, password FROM users WHERE username = ?"
        data = self.cursor.execute(request, (login,)).fetchone()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return data

    def reg_user(self, login, password):
        """Создает нового пользователя

        Args:
            login (string): Имя пользователя
            password (string): Пароль
        """

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Получим хэш пароля
        password = self.pass_encr(login, password)

        # Заносим в таблицу users пользователя с хэшированным паролем
        request = "INSERT INTO users(username, password) VALUES (?, ?)"
        self.cursor.execute(request, (login, password))
        self.connect.commit()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return True

    def check_validity(self, login, password):
        """Проверяет правильность введенных данных

        Args:
            login (string): Имя пользователя
            password (string): Пароль пользователя

        Returns:
            int: Флаг, указывающий на то, введены ли верные данные
            (0 - введённые данные верны,
            1 - пользователя с таким именем не существует,
            2 - пароль неверен)
        """

        # Достанем всю информацию о пользователе, логин которого введён
        user_data_arr = self.get_user_data(login)
        password = self.pass_encr(login, password)

        # Проверим правильность введённых пользователем данных
        if (
            user_data_arr is not None
            and user_data_arr[1] == login
            and user_data_arr[2] == password
        ):
            # Введенные данные верны
            return 0
        if user_data_arr is None:
            # Такого пользователя не существует
            return 1

        # В остальных случаях - пароль неверный
        return 2

    def get_user_id(self, login):
        """Возвращает id пользователя по его имени

        Args:
            login (string): Имя пользователя

        Returns:
            string: userID
        """

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Запрос, достающий userID из таблицы по имени пользователя
        request = "SELECT userID FROM users WHERE username = ?"
        id_arr = self.cursor.execute(request, (login,)).fetchone()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return id_arr[0]

    def pass_encr(self, user_name, user_pass):
        """Шифрованиие пароля

        Аргументы:
        userName -- имя пользователя
        userPass -- пароль, который ввел пользователь
        """
        salt = hashlib.sha256(user_name.encode()).hexdigest()

        # Хэширование пароля с солью и возвращение функцией результата
        hash_password = hashlib.sha256(user_pass.encode() + salt.encode()).hexdigest()
        return hash_password
