# sys - Используется для установки диреектории, из которой импортируются классы
import sys
import os
# sqlite3 - используется для работы с БД. Встроенная библиотека
import sqlite3
# pytest - Используется для тестирования
import pytest
sys.path.insert(0, "../")
from photo_manager import PhotoManager
import config
import unittest



def test_clean_database():
    dbPath = os.path.join(config.BASE_DIR, config.PATH_TO_DB)
    connect = sqlite3.connect(dbPath)
    cursor = connect.cursor()
    request = "DELETE from picture"
    cursor.execute(request)
    connect.commit()
    connect.close()


ERROR = "Изображение с таким именем уже существует"


@pytest.mark.parametrize(
    "meta_data, result",
    [
        (["test1", ".png", "27.12.2021 11:11:11", "Not changed", "17.1Kb",
          PhotoManager().import_pict_binary("./img/test1.png")], 'True'),
        (["test1", ".png", "27.12.2021 11:11:11", "Not changed", "17.1Kb",
          PhotoManager().import_pict_binary("./img/test1.png")], ERROR),
        (["test2", ".png", "27.12.2021 11:11:11", "Not changed", "5,5Mb",
          PhotoManager().import_pict_binary("./img/test2.png")], 'True'),
    ],
)
def test_add_image(meta_data, result):
    assert PhotoManager().add_image(meta_data, "admin") == result


@pytest.mark.parametrize(
    "image_name, result", [("test1", False), ("not_exist", True)]
)
def test_check_exist_image(image_name, result):
    assert PhotoManager().check_exist_image(image_name) == result


def test_update_image():
    assert PhotoManager().update_image("test2", ".png") is True


def test_save_image():
    assert PhotoManager().save_image("admin", "test1", "c:/python/tests/img") == True


def test_delete_image():
    assert PhotoManager().delete_image("admin", "test2") == True

def test_load_data():
    assert PhotoManager().load_data("admin") == [("test1", ".png", "27.12.2021 11:11:11", "Not changed", "17.1Kb")]

def test_take_image():
    path = "./img/test1.png"
    assert PhotoManager().take_image("admin", "test1") == PhotoManager().import_pict_binary(path)