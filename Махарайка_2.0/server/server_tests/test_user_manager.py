# import os,sys,inspect
# currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# parentdir = os.path.dirname(currentdir)
# sys.path.insert(0,parentdir)
import pytest

import sys
sys.path.insert(0, "../server")
from user_manager import UserManager
import unittest

class testUserManager(unittest.TestCase):

    def test_connectDatabase(self):
        UserManager().connect_database()

    # Ф-ция getUserData не может получить данные несуществующего пользователя
    def test_getUserData(self):
        assert UserManager().get_user_data('nonexistentUser') == None

    # Верные данные реального пользователя    
    def test_checkValidity_goodData(self):
        assert UserManager().check_validity('1', '1') == 0

    # Неправильный пароль
    def test_checkValidity_wrongPassword(self):
        assert UserManager().check_validity('1', 'wrongPassword') == 2

    # Несуществующий пользователь
    def test_checkValidity_wrongUser(self):
        assert UserManager().check_validity('nonexistentUser', '1') == 1

    # У пользователя '1' ID = 1
    def test_getUserID_goodData(self):
        assert UserManager().get_user_id(1) == 1
