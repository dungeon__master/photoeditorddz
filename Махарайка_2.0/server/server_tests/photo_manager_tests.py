import pytest
from photo_manager import PhotoManager

manager = PhotoManager()
@pytest.mark.parametrize(
    "login, result",
    [
        ("ilya", [
                ('2021-10-24_23-57-54', '.png', '2021-12-29 16:17:33', 'Not changed', '139.1Kb'),
                ('2021-10-24_23-57-5455', '.png', '2021-12-29 17:18:31', '2021-12-29 17:28:15', '36.0Kb'),
                ('2021-10-24_23-57-545', '.png', '2021-12-29 18:42:06', '2021-12-29 18:42:19', '185.5Kb')
                 ]),
        ("admin", [
                ('test_pic_2', '.png', '2021-12-29 22:24:00', 'Not changed', '84.4Kb')
                ]),
    ],
)
def test_load_data(login, result):
    assert manager.load_data(login) == result

@pytest.mark.parametrize(
    "login, image_name, result",
    [
        ('ilya', '2021-10-24_23-57-54',  "b'\\x89PNG\\"),
        ('admin', 'test_pic_2', "b'\\x89PNG\\"),
    ],
)
def test_take_image(login, image_name, result):
    bin_str = str(manager.take_image(login, image_name))
    assert bin_str.partition('r')[0] == result

@pytest.mark.parametrize(
    "login, image_name, result",
    [
        ('ilya', '2021-10-24_23-57-5455',  True),
    ],
)
def test_take_image(login, image_name, result):
    assert manager.delete_image(login, image_name) == result


