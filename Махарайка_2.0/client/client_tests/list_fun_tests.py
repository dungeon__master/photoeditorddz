import unittest
from datetime import datetime

import pytest

import config
from photo_manager import PhotoManager


# Путь к изображениям
image_path_1 = config.BASE_DIR + config.PATH_TO_IMAGES + "test_pic_1" + ".jpg"
image_path_2 = config.BASE_DIR + config.PATH_TO_IMAGES + "test_pic_2" + ".png"
# Бинарник заносимого изображения
image_1 = PhotoManager().import_pict_binary(image_path_1)
image_2 = PhotoManager().import_pict_binary(image_path_2)
# Опеределяем время занесения в БД
date = datetime.now()
time = str(date).split(".", maxsplit=1)[0]

@pytest.mark.parametrize(
    "login, new_name, old_name, extension, result",
    [
        ("1", "new_img", "test_pic_1", ".jpg", ["new_img", ".jpg", time, "Not changed", "119.0Kb", image_1]),
        ("1", "new_img", "test_pic_2", ".png", ["new_img", ".png", time, "Not changed", "84.4Kb", image_2]),
    ],
)
def test_add_image(login, new_name, old_name, extension, result):
    assert PhotoManager().add_image(login, new_name, old_name, extension) == result
