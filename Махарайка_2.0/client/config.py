import os.path

# Путь к БД
PATH_TO_DB = "./DataBase.db"

# Путь к изображениям пользователя, выгруженных из БД
PATH_TO_IMAGES = "./img/"
PATH_TO_TMP = "./tmp/"

# Путь к директории, в которой выполняется программа
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Путь до иконок изображений у кнопок фильтров
PATH_TO_BW = "./resources/black-white.jpg"
PATH_TO_NEGATIVE = "./resources/negative.jpg"
PATH_TO_SEPIA = "./resources/sepia.jpg"
