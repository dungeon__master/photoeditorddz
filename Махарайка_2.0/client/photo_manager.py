# os - Используется для определения пути для сохрения или загрузки из-я
import os

# datetime - Используется для определения времени создания и редактирования из-я
from datetime import datetime

from list import UiList
import config


class PhotoManager:
    """Взаимодействует с изображениями

    Args:
        Database (class): база данных
    """

    def import_pict_binary(self, image_path):
        """Переводит изображения в BLOB

        Args:
            image_path (string): путь к изображению

        Returns:
            BLOB: изображение в формате BLOB
        """

        # Взаимодействуем с изображением побайтно
        with open(image_path, "rb") as file:
            pict_binary = file.read()

        # Закрываем поток
        file.close()

        # Возвращение бинарника изображения
        return pict_binary

    def write_to_file(self, data, file_path):
        """Запись из БД по заданному пути

        Args:
            data (BLOB): бинарник изображения
            file_path (string): путь для записи изображения
        """

        with open(file_path, "wb") as file:
            file.write(data)

    # Перевод размера файла в необходимый формат
    def get_size(self, __size):
        """Получает размер изображения

        Args:
            __size (int): Размер в байтах

        Returns:
            str: Строка с информацией о размере изображения
        """

        # Рассчет размера изображение и вывод в его оптимальной мере размера
        # Изначально - размер файла в байтах
        fullsize = str(__size) + "Bytes"

        # Если размер > 2^10, то повысим степень и переведем изображение в килобайты
        if __size > 1024:
            __size = round(__size / 1024 * 10) / 10
            fullsize = str(__size) + "Kb"

        # Если все ещё размер > 2^10, то повысим степень и переведем изображение в мегабайты
        if __size > 1024:
            __size = round(__size / 1024 * 10) / 10
            fullsize = str(__size) + "Mb"
        return fullsize

    def add_image(self, __login, new_name="", old_name="", extension=""):
        """Добавляет изображение в БД

        Args:
            __login (string): имя пользователя
            old_name (string, optional): действительное имя изображения
                                            по умолч. ''
            new_name (string, optional): имя изображения,
            под которым хочет сохранить его пользователь
                                       по умолч. ''
            extension (string, optional): расширение изображения
                                           по умолч. ''

        Returns:
            mixed: обработка ошибок
        """
        if new_name == "":
            # Если параметры функции не заданы, то спрашиваем путь до файла у пользователя
            list_form = UiList()
            image_path = list_form.get_file_name()

            file_name, image_extension = os.path.splitext(image_path)
            name = file_name.split("/")[-1]
        else:
            # Сохранение изображения в БД
            image = config.PATH_TO_IMAGES + old_name + extension
            image_path = os.path.join(config.BASE_DIR, image)
            name = new_name
            image_extension = extension

        # Если пользователь нажал Отмена или Закрыть окно
        if image_path is None or image_path == "":
            return False

        image = self.import_pict_binary(image_path)
        # Получение размера файла
        size = self.get_size(os.path.getsize(image_path))

        # Опеределяем время занесения в БД
        date = datetime.now()

        # Обрезание милисекунд
        time = str(date).split(".", maxsplit=1)[0]

        # Массив с мета-данными изображения
        message = [name, image_extension, time, "Not changed", size, image]
        return message
