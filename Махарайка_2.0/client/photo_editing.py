# -*- coding: utf-8 -*-
import os
from PyQt5 import QtCore, QtGui, QtWidgets
import config



class UiEditForm:
    """Представление формы изменения изображения"""

    def __init__(self):
        self.centralwidget = None
        self.contrast_scroll_bar = None
        self.sharpness_scroll_bar = None
        self.back_to_list_button = None
        self.image_label = None
        self.contrast_label = None
        self.sharpness_label = None
        self.save_edits_button = None
        self.cancel_edits_button = None
        self.filters_label = None
        self.black_white_button = None
        self.negative_button = None
        self.sepia_button = None
        self.black_white_picture = None
        self.negative_picture = None
        self.sepia_picture = None

    def setup_ui(self, edit_form):
        """Формирует графическую форму

        Args:
            edit_form: объект графической формы
        """

        # Параметры главного окна
        edit_form.setObjectName("edit_form")
        edit_form.resize(908, 670)
        edit_form.setMaximumSize(QtCore.QSize(908, 670))
        edit_form.setMinimumSize(QtCore.QSize(908, 670))
        edit_form.setStyleSheet("background-color: rgb(189, 189, 189);")

        # Параметры области расположения графических объектов
        self.centralwidget = QtWidgets.QWidget(edit_form)
        self.centralwidget.setObjectName("centralwidget")

        # Параметры бегунка изменения контраста
        self.contrast_scroll_bar = QtWidgets.QScrollBar(self.centralwidget)
        self.contrast_scroll_bar.setGeometry(QtCore.QRect(570, 130, 331, 21))
        font = QtGui.QFont()
        font.setPointSize(2)
        self.contrast_scroll_bar.setFont(font)
        self.contrast_scroll_bar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.contrast_scroll_bar.setStyleSheet("background-color: rgb(85, 0, 0);")
        self.contrast_scroll_bar.setMinimum(1)
        self.contrast_scroll_bar.setValue(1)
        self.contrast_scroll_bar.setMaximum(101)
        self.contrast_scroll_bar.setPageStep(10)
        self.contrast_scroll_bar.setOrientation(QtCore.Qt.Horizontal)
        self.contrast_scroll_bar.setObjectName("contrast_scroll_bar")

        # Параметры бегунка изменения четкости
        self.sharpness_scroll_bar = QtWidgets.QScrollBar(self.centralwidget)
        self.sharpness_scroll_bar.setGeometry(QtCore.QRect(570, 210, 331, 21))
        self.sharpness_scroll_bar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.sharpness_scroll_bar.setStyleSheet("background-color: rgb(85, 0, 0);")
        self.sharpness_scroll_bar.setMinimum(0)
        self.sharpness_scroll_bar.setValue(1)
        self.sharpness_scroll_bar.setMaximum(101)
        self.sharpness_scroll_bar.setPageStep(10)
        self.sharpness_scroll_bar.setOrientation(QtCore.Qt.Horizontal)
        self.sharpness_scroll_bar.setObjectName("sharpness_scroll_bar")

        # Параметры кнопки возвращения
        self.back_to_list_button = QtWidgets.QPushButton(self.centralwidget)
        self.back_to_list_button.setGeometry(QtCore.QRect(570, 10, 323, 61))
        font = QtGui.QFont()
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        self.back_to_list_button.setFont(font)
        self.back_to_list_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.back_to_list_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(170, 0, 255);\n"
            "color: rgb(255, 0, 0);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(85, 0, 127);\n"
            "color: rgb(255, 0, 0);\n"
            "border-radius: 11px;}"
        )
        self.back_to_list_button.setObjectName("back_to_list_button")

        # Параметры блоков для расположения изображений
        self.image_label = QtWidgets.QLabel(self.centralwidget)
        self.image_label.setGeometry(QtCore.QRect(10, 10, 541, 651))
        self.image_label.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.image_label.setText("")
        self.image_label.setScaledContents(True)
        self.image_label.setObjectName("image_label")

        # Параметры подписи бегунка изменения контраста
        self.contrast_label = QtWidgets.QLabel(self.centralwidget)
        self.contrast_label.setGeometry(QtCore.QRect(570, 80, 159, 41))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(23)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.contrast_label.setFont(font)
        self.contrast_label.setObjectName("contrast_label")

        # Параметры подписи бегунка изменениня четкости
        self.sharpness_label = QtWidgets.QLabel(self.centralwidget)
        self.sharpness_label.setGeometry(QtCore.QRect(570, 160, 151, 41))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(23)
        font.setBold(True)
        font.setWeight(75)
        self.sharpness_label.setFont(font)
        self.sharpness_label.setObjectName("sharpness_label")

        # Параметры кнопки сохранения внесённых в изображение изменений
        self.save_edits_button = QtWidgets.QPushButton(self.centralwidget)
        self.save_edits_button.setGeometry(QtCore.QRect(560, 600, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.save_edits_button.setFont(font)
        self.save_edits_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.save_edits_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(0, 170, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(0, 85, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            ""
        )
        self.save_edits_button.setObjectName("save_edits_button")

        # Параметры кнопки сброса внесенных в изображение изменений
        self.cancel_edits_button = QtWidgets.QPushButton(self.centralwidget)
        self.cancel_edits_button.setGeometry(QtCore.QRect(740, 600, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.cancel_edits_button.setFont(font)
        self.cancel_edits_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.cancel_edits_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(255, 0, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(170, 0, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            ""
        )
        self.cancel_edits_button.setObjectName("cancel_edits_button")

        # Параметры подписи блока с кнопками применения фильтров
        self.filters_label = QtWidgets.QLabel(self.centralwidget)
        self.filters_label.setGeometry(QtCore.QRect(570, 230, 231, 71))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        self.filters_label.setFont(font)
        self.filters_label.setObjectName("filters_label")

        # Параметры кнопки применения фильтра "черно-белый"
        self.black_white_button = QtWidgets.QPushButton(self.centralwidget)
        self.black_white_button.setGeometry(QtCore.QRect(560, 310, 171, 61))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.black_white_button.setFont(font)
        self.black_white_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.black_white_button.setMouseTracking(False)
        self.black_white_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: qlineargradient"
            "(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), "
            "stop:1 rgba(255, 255, 255, 255));\n "
            "color: rgb(170, 0, 0);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(48, 48, 48);\n"
            "color: rgb(170, 0, 0);\n"
            "border-radius: 11px;}"
        )
        self.black_white_button.setObjectName("black_white_button")

        # Параметры кнопки применения фильтра "негатив"
        self.negative_button = QtWidgets.QPushButton(self.centralwidget)
        self.negative_button.setGeometry(QtCore.QRect(560, 400, 171, 61))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.negative_button.setFont(font)
        self.negative_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.negative_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: qlineargradient"
            "(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 127, 255), "
            "stop:1 rgba(255, 255, 255, 255));\n "
            "color: rgb(0, 0, 0);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(0, 0, 152);\n"
            "color: rgb(0, 0, 0);\n"
            "border-radius: 11px;}"
        )
        self.negative_button.setObjectName("negative_button")

        # Параметры кнопки применения фильтра "сепия"
        self.sepia_button = QtWidgets.QPushButton(self.centralwidget)
        self.sepia_button.setGeometry(QtCore.QRect(560, 490, 171, 61))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.sepia_button.setFont(font)
        self.sepia_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.sepia_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: qlineargradient"
            "(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(180, 144, 0, 255), "
            "stop:1 rgba(255, 255, 255, 255));\n "
            "color: rgb(255, 0, 127);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(255, 206, 8);\n"
            "color: rgb(255, 0, 127);\n"
            "border-radius: 11px;}"
        )
        self.sepia_button.setObjectName("sepia_button")

        # Параметры превью-картинки фильтра "черно-белый"
        self.black_white_picture = QtWidgets.QLabel(self.centralwidget)
        self.black_white_picture.setGeometry(QtCore.QRect(750, 310, 61, 61))
        self.black_white_picture.setStyleSheet("border: 2px solid;")
        self.black_white_picture.setPixmap(QtGui.QPixmap("" + config.PATH_TO_BW + ""))
        self.black_white_picture.setScaledContents(True)
        self.black_white_picture.setObjectName("black_white_picture")

        # Параметры превью-картинки фильтра "негатив"
        self.negative_picture = QtWidgets.QLabel(self.centralwidget)
        self.negative_picture.setGeometry(QtCore.QRect(750, 400, 61, 61))
        self.negative_picture.setPixmap(
            QtGui.QPixmap("" + config.PATH_TO_NEGATIVE + "")
        )
        self.negative_picture.setStyleSheet("border: 2px solid;")
        self.negative_picture.setScaledContents(True)
        self.negative_picture.setObjectName("negative_picture")

        # Параметры превью-картинки фильтра "сепия"
        self.sepia_picture = QtWidgets.QLabel(self.centralwidget)
        self.sepia_picture.setGeometry(QtCore.QRect(750, 490, 61, 61))
        self.sepia_picture.setPixmap(QtGui.QPixmap("" + config.PATH_TO_SEPIA + ""))
        self.sepia_picture.setStyleSheet("border: 2px solid;")
        self.sepia_picture.setScaledContents(True)
        self.sepia_picture.setObjectName("sepia_picture")
        edit_form.setCentralWidget(self.centralwidget)

        self.retranslate_ui(edit_form)
        QtCore.QMetaObject.connectSlotsByName(edit_form)

    def retranslate_ui(self, edit_form):
        """Заполнение объектов записями

        Args:
            edit_form (object): объект графической формы
        """

        _translate = QtCore.QCoreApplication.translate
        edit_form.setWindowTitle(_translate("edit_form", "PhotoEdit"))
        self.back_to_list_button.setText(_translate("edit_form", "Дать заднюю"))
        self.contrast_label.setText(_translate("edit_form", "Контраст:"))
        self.sharpness_label.setText(_translate("edit_form", "Резкость:"))
        self.save_edits_button.setText(_translate("edit_form", "Применить"))
        self.cancel_edits_button.setText(_translate("edit_form", "Отменить"))
        self.filters_label.setText(_translate("edit_form", "Фильтры:"))
        self.black_white_button.setText(_translate("edit_form", "Чёрно-белый"))
        self.negative_button.setText(_translate("edit_form", "Негатив"))
        self.sepia_button.setText(_translate("edit_form", "Сепия"))

    def show_image(self, image):
        """Выводит выбранное изображение в окно редактирования

        Args:
            image (object): выбранное изображение
        """

        image = config.PATH_TO_IMAGES + image.name + image.extension
        full_path = os.path.join(config.BASE_DIR, image)
        pixmap = QtGui.QPixmap(full_path)
        self.image_label.setPixmap(pixmap)
