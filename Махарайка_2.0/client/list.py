# os - Используются для определения пути для сохрения или загрузки из-я
import os

# PyQt5 - используется для создание графического интерфейса
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

import config


class PhotoClass:
    """Информация о выбранном изображении"""

    def __init__(self, name=None, extension=None):
        self.name = name
        self.extension = extension


# Класс, содержащий графическое окно с таблицей, содержащей информацию о фото
class UiList(QDialog):
    """Представляет графическую форму, содержащую информацию о изображениях пользователя

    Args:
        QDialog (class): позволяет работать с диалоговыми окнами
    """

    # Установка корректного пути к базу данных
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    dbPath = os.path.join(BASE_DIR, config.PATH_TO_DB)

    def __init__(self):
        self.centralwidget = None
        self.delete_record_button = None
        self.edit_record_button = None
        self.upload_button = None
        self.load_button = None
        self.table_widget = None
        # Ссылаемся на PhotoClass и вызываем его конструктор
        super().__init__(None)
        # Создаем объект класса
        self.image = PhotoClass()

    def setup_ui(self, list_form):
        """Формирует графическую форму

        Args:
            list_form: объект графической формы
        """

        # Параметры главного окна
        list_form.setObjectName("list_form")
        list_form.resize(899, 596)
        list_form.setMaximumSize(QtCore.QSize(899, 596))
        list_form.setMinimumSize(QtCore.QSize(899, 596))
        list_form.setStyleSheet("background-color: rgb(130, 130, 130);")

        # Параметры области расположения графических объектов
        self.centralwidget = QtWidgets.QWidget(list_form)
        self.centralwidget.setObjectName("centralwidget")

        # Параметры кнопки удаления изображения из базы
        self.delete_record_button = QtWidgets.QPushButton(self.centralwidget)
        self.delete_record_button.setGeometry(QtCore.QRect(20, 540, 180, 42))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.delete_record_button.setFont(font)

        # Устанавливаем вид курсора при наведении на объект
        self.delete_record_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.delete_record_button.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(255, 0, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius:11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(170, 0, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius:11px;}"
        )
        self.delete_record_button.setObjectName("delete_record_button")

        # Параметры кнопки редактирования изображения
        self.edit_record_button = QtWidgets.QPushButton(self.centralwidget)
        self.edit_record_button.setGeometry(QtCore.QRect(240, 540, 180, 42))

        # Задаем шрифт
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.edit_record_button.setFont(font)

        # Устанавливаем вид курсора при наведении на объект
        self.edit_record_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.edit_record_button.setStyleSheet(
            "QPushButton{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(255, 170, 0);\n"
            "border-radius:11px;}\n"
            "QPushButton:hover{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(255, 85, 0);\n"
            "border-radius:11px;}"
        )
        self.edit_record_button.setObjectName("edit_record_button")

        # Параметры кнопки выгрузки изображения из базы
        self.upload_button = QtWidgets.QPushButton(self.centralwidget)
        self.upload_button.setGeometry(QtCore.QRect(480, 540, 180, 42))

        # Задаем шрифт
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.upload_button.setFont(font)

        # Устанавливаем вид курсора при наведении на объект
        self.upload_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.upload_button.setStyleSheet(
            "QPushButton{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(0, 0, 255);\n"
            "border-radius:11px;}\n"
            "QPushButton:hover{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(0, 0, 127);\n"
            "border-radius:11px;}"
        )
        self.upload_button.setObjectName("upload_button")

        # Параметры кнопки подгрузки изображения в базу
        self.load_button = QPushButton(self.centralwidget)
        self.load_button.setGeometry(QtCore.QRect(700, 540, 180, 42))
        font = QtGui.QFont()

        # Задаем шрифт
        font.setBold(True)
        font.setWeight(75)
        self.load_button.setFont(font)

        # Устанавливаем вид курсора при наведении на объект
        self.load_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.load_button.setStyleSheet(
            "QPushButton{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(0, 170, 0);\n"
            "border-radius:11px;}\n"
            "QPushButton:hover{\n"
            "color: rgb(255, 255, 255);\n"
            "background-color: rgb(0, 85, 0);\n"
            "border-radius:11px;}"
        )
        self.load_button.setObjectName("load_button")

        # Параметры таблицы с информацией о изображениях в базе
        self.table_widget = QtWidgets.QTableWidget(self.centralwidget)

        # Разрешает взаимодействие с таблицей
        self.table_widget.setEnabled(True)

        # Задаем вид таблицы
        self.table_widget.setGeometry(QtCore.QRect(10, 10, 881, 521))

        # Задаем шрифт
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setItalic(True)
        self.table_widget.setFont(font)

        # Устанавливаем вид курсора при наведении на таблицу
        self.table_widget.viewport().setProperty(
            "cursor", QtGui.QCursor(QtCore.Qt.PointingHandCursor)
        )
        self.table_widget.setMouseTracking(False)
        self.table_widget.setAutoFillBackground(True)
        self.table_widget.setStyleSheet("background-color: rgb(255, 255, 255);")

        # Отключение горизонтальной прокрутки
        self.table_widget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        # Подстраиваем размеры полей под контент в них
        self.table_widget.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents
        )

        # Отключаем триггеры на изменение информации таблицы
        self.table_widget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # Выбор строки с помощью клавиатуры
        self.table_widget.setTabKeyNavigation(True)

        # Запрет перетаскивания элементов таблицы
        self.table_widget.setDragEnabled(False)
        self.table_widget.setDragDropOverwriteMode(False)

        # Чередование раскраски строк
        self.table_widget.setAlternatingRowColors(True)

        # Нажатием можно выбрать только одну строку
        self.table_widget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table_widget.setObjectName("table_widget")

        # Устанавливаем количество колонок таблицы
        self.table_widget.setColumnCount(5)

        # Параметры шапки таблицы:
        #
        # "Название"
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setVerticalHeaderItem(0, item)
        #
        # "Расширение"
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setVerticalHeaderItem(1, item)
        #
        # "Время добавления"
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setVerticalHeaderItem(2, item)
        #
        # "Время редактирования"
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setVerticalHeaderItem(3, item)
        #
        # "Размер"
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setVerticalHeaderItem(4, item)

        # Параметры шапки таблицы по горизонтали
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.table_widget.horizontalHeader().setCascadingSectionResizes(False)
        self.table_widget.horizontalHeader().setDefaultSectionSize(172)
        list_form.setCentralWidget(self.centralwidget)
        self.retranslate_ui(list_form)
        QtCore.QMetaObject.connectSlotsByName(list_form)

    def retranslate_ui(self, list_form):
        """Заполнение объектов записями

        Args:
            list_form: объект графической формы
        """

        _translate = QtCore.QCoreApplication.translate
        list_form.setWindowTitle(_translate("list_form", "Информация об изображениях"))
        self.delete_record_button.setText(_translate("list_form", "Удалить"))
        self.edit_record_button.setText(_translate("list_form", "Редактировать"))
        self.upload_button.setText(_translate("list_form", "Выгрузить"))
        self.load_button.setText(_translate("list_form", "Загрузить"))
        item = self.table_widget.horizontalHeaderItem(0)
        item.setText(_translate("list_form", "Название"))
        item = self.table_widget.horizontalHeaderItem(1)
        item.setText(_translate("list_form", "Расширение"))
        item = self.table_widget.horizontalHeaderItem(2)
        item.setText(_translate("list_form", "Время добавления"))
        item = self.table_widget.horizontalHeaderItem(3)
        item.setText(_translate("list_form", "Время редактирования"))
        item = self.table_widget.horizontalHeaderItem(4)
        item.setText(_translate("list_form", "Размер"))
        self.table_widget.clicked.connect(self.clicked_table)

    def clicked_table(self):
        """Получение информации из строки таблицы при нажатии на нее"""

        # Список индексов выделенных элементов(ячеек)
        index = self.table_widget.selectedIndexes()
        # Получаем номер строки, в которой находятся выбранные ячейки
        row_number = index[0].row()
        # Получаем позиции ячеек с названием и расширением выбранного изображения
        image_name = self.table_widget.item(row_number, 0)
        image_extension = self.table_widget.item(row_number, 1)

        # Если пользователь не выбрал строку
        if image_name is None:
            self.image.name = None
            self.image.extension = None

        # Если строка выбрана
        else:
            self.image.name = image_name.text()
            self.image.extension = image_extension.text()

    def get_file_name(self):
        """Вызывает диалоговое окно для выбора изображений

        Returns:
            [string]: путь до выбранного изображения
        """
        res = QFileDialog.getOpenFileName(
            self,
            "Open File",
            "C:/",
            "JPG File (*.jpg);JPEG File (*.jpeg);PNG File (*.png)",
        )
        return res[0]

    def load_data(self, info):
        """Заполняет таблицу информацией из БД

        Args:
            info (arr): Массив с данными о загруженных фото пользователя
        """
        # Подсчет количества строк, необходимых для генерации на интерфейсе
        size = len(info)

        # Создание строк на экране
        table_row = 0
        self.table_widget.setRowCount(size)
        for row in info:
            self.table_widget.setItem(table_row, 0, QtWidgets.QTableWidgetItem(row[0]))
            self.table_widget.setItem(table_row, 1, QtWidgets.QTableWidgetItem(row[1]))
            self.table_widget.setItem(table_row, 2, QtWidgets.QTableWidgetItem(row[2]))
            self.table_widget.setItem(table_row, 3, QtWidgets.QTableWidgetItem(row[3]))
            self.table_widget.setItem(table_row, 4, QtWidgets.QTableWidgetItem(row[4]))
            table_row = table_row + 1

    def get_dir(self):
        """Выбор директории для выгрузки изображения

        Returns:
            [string]: полный путь к папке
        """

        res = QFileDialog.getExistingDirectory(self, "Выбрать папку", ".")
        return res
