# os, shutil - Используются для определения пути для сохрения или загрузки из-я
import os
import shutil

# socket, threading - модули, использованные для реализации клиент-серверной архитектуры
import socket
from threading import Thread
import sys
import time
import marshal

# pyqt5 - библиотека для создания графического интерфейса
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

import config
from list import UiList
from login_form import UiLoginForm
from photo_editing import UiEditForm
from photo_manager import PhotoManager
from save_changes_form import UiSaveNameForm

SERVER = "127.0.0.1"
PORT = 1356
LOGIN = ""
DATA = ""
MESSAGE = ""
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((SERVER, PORT))


def main():
    global DATA
    # Отображение формы авторизации
    app = QtWidgets.QApplication(sys.argv)
    login_win = QtWidgets.QMainWindow()
    login_form = UiLoginForm()
    login_form.setup_ui(login_win)
    login_win.show()

    # Переход к окну со списком изображений
    def open_list_form():
        """Переходит к окну со списком изображений"""

        global list_win
        global LOGIN
        global MESSAGE
        list_win = QtWidgets.QMainWindow()

        # Отрисовка графического интерфейса таблицы с изображений
        list_form = UiList()
        list_form.setup_ui(list_win)
        login_win.close()
        MESSAGE = ["update", LOGIN]
        time.sleep(1)
        list_form.load_data(DATA)
        list_win.show()

        # Обработка событий при нажатии на кнопки
        #
        # Переход к изменению изображению
        list_form.edit_record_button.clicked.connect(
            lambda: load_image_to_file(list_form)
        )
        #
        # Загрузка изображения
        list_form.load_button.clicked.connect(lambda: add_image(list_form))
        #
        # Удаление изображения
        list_form.delete_record_button.clicked.connect(lambda: delete_image(list_form))
        #
        # Выгрузка изображения
        list_form.upload_button.clicked.connect(lambda: save_image(list_form))

    def open_edit_form(list_form):
        """Открывает окно редактирования сообщений

        Args:
            list_form (UiList): объект графического интерфейса окна с таблицей
        """

        global edit_win
        edit_win = QtWidgets.QMainWindow()
        edit_form = UiEditForm()
        edit_form.setup_ui(edit_win)
        list_win.close()
        edit_win.show()
        edit_form.show_image(list_form.image)

        # Обработка нажатия кнопок фильтра
        edit_form.black_white_button.clicked.connect(
            lambda: make_black_white(list_form, edit_form)
        )
        edit_form.sepia_button.clicked.connect(
            lambda: make_sepia(list_form, edit_form)
        )
        edit_form.negative_button.clicked.connect(
            lambda: make_negative(list_form, edit_form)
        )

        # Обработаем изменение показателя бегунка контраста
        edit_form.contrast_scroll_bar.valueChanged.connect(
            lambda: make_contrast_sharpness(
                list_form.image,
                edit_form.contrast_scroll_bar.value(),
                edit_form.sharpness_scroll_bar.value(),
                edit_form,
            )
        )
        # Обработаем изменение показателя бегунка контраста
        edit_form.sharpness_scroll_bar.valueChanged.connect(
            lambda: make_contrast_sharpness(
                list_form.image,
                edit_form.contrast_scroll_bar.value(),
                edit_form.sharpness_scroll_bar.value(),
                edit_form,
            )
        )

        # Возвращение на форму со списком изображений
        edit_form.back_to_list_button.clicked.connect(
            lambda: return_to_list(list_form)
        )
        edit_form.save_edits_button.clicked.connect(
            lambda: open_save_changes_form(list_form)
        )
        edit_form.cancel_edits_button.clicked.connect(
            lambda: canceling(LOGIN, list_form, edit_form)
        )
        app.aboutToQuit.connect(lambda: event_close(list_form))

    def event_close(list_form):
        """Удаление изображения из img и папки tmp

        Args:
            list_form: объект графической формы
        """
        global MESSAGE
        image = (
                config.PATH_TO_IMAGES + list_form.image.name + list_form.image.extension
        )
        MESSAGE = ["clean", image]
        image_path = os.path.join(config.BASE_DIR, image)
        tmp_path = os.path.join(config.BASE_DIR, config.PATH_TO_TMP)
        try:
            # Удаление изображения из папки img
            os.remove(image_path)
            # Удаление папки tmp со всем содержимым
            shutil.rmtree(tmp_path)
        except:
            return False

    def return_to_list(list_form):
        """Возвращение к окну выбора изображения

        Args:
            list_form (UiList): объект графического интерфейса окна с таблицей
        """

        global LOGIN, MESSAGE

        # Удаление изображения
        event_close(list_form)
        MESSAGE = ["update", LOGIN]
        time.sleep(1)
        list_form.load_data(DATA)
        edit_win.close()
        list_win.show()

    def open_save_changes_form(list_form):
        """Форма для ввода имени изображения

        Args:
            list_form: объект графической формы

        Returns:

        """
        global save_name_form

        # Создание графической формы сохранения изображения
        save_name_form = QtWidgets.QMainWindow()
        saving_form = UiSaveNameForm()
        saving_form.setup_ui(save_name_form)
        saving_form.new_name_edit.setText(list_form.image.name)
        save_name_form.show()

        # Обработка нажатия на кнопку сохранения
        saving_form.save_new_name_but.clicked.connect(
            lambda: save_changed_image(save_name_form, saving_form, list_form)
        )

    def error_window(error):
        """Окно с ошибкой

        Args:
            error (string): текст ошибки
        """
        global msg

        # Формирование окна с ошибкой
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText(error)
        msg.setWindowTitle("Error")
        msg.show()

    def load_image_to_file(list_form):
        """Загрузка изображения в папку img

        Args:
            list_form (ui_list): объект графической формы
        """
        global MESSAGE, LOGIN
        image_name = list_form.image.name
        extension = list_form.image.extension
        if image_name is None:
            error_window("Пожалуйста, выберите изображение")
        else:
            MESSAGE = ["edit", LOGIN, image_name, extension]
            time.sleep(1)
            PhotoManager().write_to_file(
                DATA, config.PATH_TO_IMAGES + image_name + extension
            )
            open_edit_form(list_form)

    def add_image(list_form):
        """Реализует кнопку добавления изображения в БД

        Args:
            list_form: объект графической формы
        """
        global MESSAGE, LOGIN
        res = PhotoManager().add_image(LOGIN)

        if res is not False:
            MESSAGE = ["add", res, LOGIN]
            time.sleep(0.7)
            if DATA != "True":
                error_window(DATA)
            else:
                MESSAGE = ["update", LOGIN]
                time.sleep(0.5)
                list_form.load_data(DATA)

    def delete_image(list_form):
        """Реализует кнопку удаления изображения из БД

        Args:
            list_form: объект графической формы
        """
        global MESSAGE, LOGIN
        MESSAGE = ["delete", LOGIN, list_form.image.name]
        time.sleep(0.7)
        if list_form.image.name is None:
            error_window("Выберите картинку")
        else:
            MESSAGE = ["update", LOGIN]
            time.sleep(0.5)
            list_form.load_data(DATA)

    def save_image(list_form):
        """Выгружает изображение в выбранную папку

        Args:
            list_form: объект графической формы
        """
        global MESSAGE, LOGIN
        if list_form.image.name is None:
            error_window("Выберите картинку")
        else:
            MESSAGE = ["save", LOGIN, list_form.image.name]
            time.sleep(0.7)
            path = list_form.get_dir()
            if path != '':
                image_path = (
                        path
                        + "/"
                        + list_form.image.name
                        + list_form.image.extension
                )
                PhotoManager().write_to_file(DATA, image_path)

    def make_contrast_sharpness(image, coef_contrast, coef_sharp, edit_form):
        """Применяет к изображению показатель бегунка контраста

        Args:
            image (object): объект изображения
            coef_contrast (int): степень контраста (
            по умолчаю - 1,
             менее 1 - уменьшенный контраст,
              более 1 - увеличенный контраст)
            coef_sharp (int): степень резкости (по умолчаю - 1,
             менее 1 - уменьшенная резкость,
              более 1 - увеличенная резкость)
            edit_form: объект формы окна редактирования
        """
        global MESSAGE
        picture_full_name = image.name + image.extension
        MESSAGE = [
            "contrast_sharpness",
            picture_full_name,
            coef_contrast,
            coef_sharp,
        ]
        time.sleep(0.5)
        PhotoManager().write_to_file(DATA, config.PATH_TO_IMAGES + picture_full_name)
        edit_form.show_image(image)

    def make_black_white(list_form, edit_form):
        """Переводит изображение в ЧБ

        Args:
            list_form (UiList): объект графической формы списка изображений
            edit_form (UiEditForm): объект графической формы изменения изображений
        """
        global MESSAGE
        picture_full_name = list_form.image.name + list_form.image.extension
        MESSAGE = ["b&w", picture_full_name]
        time.sleep(0.5)
        PhotoManager().write_to_file(DATA, config.PATH_TO_IMAGES + picture_full_name)
        edit_form.show_image(list_form.image)

    def make_sepia(list_form, edit_form):
        """Переводит изображение в сепию

        Args:
            list_form (UiList): объект графической формы списка изображений
            edit_form (UiEditForm): объект графической формы изменения изображений
        """
        global MESSAGE
        picture_full_name = list_form.image.name + list_form.image.extension
        MESSAGE = ["sepia", picture_full_name]
        time.sleep(0.5)
        PhotoManager().write_to_file(DATA, config.PATH_TO_IMAGES + picture_full_name)
        edit_form.show_image(list_form.image)

    def make_negative(list_form, edit_form):
        """Переводит изображение в негатив

        Args:
            list_form (UiList): объект графической формы списка изображений
            edit_form (UiEditForm): объект графической формы изменения изображений
        """

        global MESSAGE
        picture_full_name = list_form.image.name + list_form.image.extension
        MESSAGE = ["negative", picture_full_name]
        time.sleep(0.5)
        PhotoManager().write_to_file(DATA, config.PATH_TO_IMAGES + picture_full_name)
        edit_form.show_image(list_form.image)

    def canceling(login, list_form, edit_form):
        """Отменяет примененные фильтры

        Args:
            login (string): имя пользователя
            list_form (UiList): объект графической формы списка изображений
            edit_form (UiEditForm): объект графической формы редактирования изображений
        """
        global MESSAGE, LOGIN
        picture_full_name = list_form.image.name + list_form.image.extension
        MESSAGE = ["cancel", LOGIN, list_form.image.name, list_form.image.extension]
        time.sleep(0.5)
        # Сохраняет в папку первичное изображение до применения по нему фильтров
        PhotoManager().write_to_file(DATA, config.PATH_TO_IMAGES + picture_full_name)
        edit_form.show_image(list_form.image)
        edit_form.contrast_scroll_bar.setValue(1)
        edit_form.sharpness_scroll_bar.setValue(1)

    def save_changed_image(saving_win, saving_form, list_form):
        """Осуществляется сохранение измененного изображения в БД

        Args:
            saving_win (object): окно сохранения изображений
            saving_form (UiSaveNameForm): графическая форма сохранения изображений
            list_form (UiList): графическая форма со списком изображений
        """

        global MESSAGE, LOGIN

        # Получение информации об изображении
        old_name = list_form.image.name
        extension = list_form.image.extension
        new_name = saving_form.new_name_edit.text()
        MESSAGE = [
            "save_change",
            PhotoManager().add_image(LOGIN, new_name, old_name, extension),
            LOGIN,
        ]
        time.sleep(0.5)
        saving_win.close()
        return_to_list(list_form)

    def auth():
        global MESSAGE, LOGIN
        # Получение данных из полей
        password = login_form.password_edit.text()
        LOGIN = login_form.login_edit.text()
        if LOGIN == "" or password == "":
            login_form.warnings.setText("Введены неверные данные!!!")
        else:
            MESSAGE = ["login", LOGIN, password]
            time.sleep(0.8)
            if DATA == "True":
                open_list_form()
            elif DATA == "False":
                login_form.warnings.setText("Введёны неверные данные!!!")

    login_form.login_but.clicked.connect(auth)
    sys.exit(app.exec_())


def write():
    """Возможность отправки сообщения серверу"""
    global MESSAGE
    while True:
        if MESSAGE != "":
            output_data = marshal.dumps(MESSAGE)
            client.sendall(output_data)
            MESSAGE = ""


def listen():
    """Возможность слушать сообщения сервера"""
    global DATA
    while True:
        input_data = client.recv(3072000)
        data = marshal.loads(input_data)
        if len(data) < 1000:
            print("Сервер пишет: ", data)
        else:
            print("Раньше здесь был мат")
        DATA = data


# Помещаем наши функции в отдельные потоки, чтобы все работало одновременно
main_stream = Thread(target=main)
main_stream.start()
writing_stream = Thread(target=write)
writing_stream.start()
listening_stream = Thread(target=listen)
listening_stream.start()
