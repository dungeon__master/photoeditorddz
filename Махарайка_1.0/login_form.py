from PyQt5 import QtCore, QtGui, QtWidgets


class UiLoginForm:
    """Представление формы авторизации пользователя
    """

    def setup_ui(self, login_form):
        """Формирует графическую форму

        Args:
            login_form: объект графической формы
        """

        # Параметры главного окна
        login_form.setObjectName("login_form")
        login_form.setEnabled(True)
        login_form.resize(590, 362)
        login_form.setMinimumSize(QtCore.QSize(590, 362))
        login_form.setMaximumSize(QtCore.QSize(590, 362))
        font = QtGui.QFont()
        font.setPointSize(31)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        login_form.setFont(font)
        login_form.setMouseTracking(False)
        login_form.setLayoutDirection(QtCore.Qt.RightToLeft)
        login_form.setStyleSheet("background-color: rgb(85, 85, 127);")

        # Параметры области расположения графических объектов
        self.widget = QtWidgets.QWidget(login_form)
        self.widget.setStyleSheet("background-color: rgb(173, 173, 173);")
        self.widget.setObjectName("widget")

        # Параметры заголовка формы
        self.title = QtWidgets.QLabel(self.widget)
        self.title.setGeometry(QtCore.QRect(0, 0, 580, 70))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(18)
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.title.setFont(font)
        self.title.setMouseTracking(True)
        self.title.setStyleSheet("")
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setWordWrap(False)
        self.title.setObjectName("title")

        # Параметры подписи поля ввода логина
        self.login_label = QtWidgets.QLabel(self.widget)
        self.login_label.setGeometry(QtCore.QRect(30, 80, 120, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(20)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.login_label.setFont(font)
        self.login_label.setMouseTracking(True)
        self.login_label.setObjectName("login_label")

        # Параметры подписи поля ввода пароля
        self.password_label = QtWidgets.QLabel(self.widget)
        self.password_label.setGeometry(QtCore.QRect(30, 150, 131, 51))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.password_label.setFont(font)
        self.password_label.setMouseTracking(True)
        self.password_label.setObjectName("password_label")

        # Параметры кнопки авторизации
        self.login_but = QtWidgets.QPushButton(self.widget)
        self.login_but.setEnabled(True)
        self.login_but.setGeometry(QtCore.QRect(40, 252, 520, 52))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.login_but.setFont(font)
        self.login_but.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.login_but.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(0, 170, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(0, 85, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            ""
        )
        self.login_but.setIconSize(QtCore.QSize(16, 16))
        self.login_but.setObjectName("login_but")

        # Параметры поля ввода логина
        self.login_edit = QtWidgets.QLineEdit(self.widget)
        self.login_edit.setGeometry(QtCore.QRect(170, 90, 381, 41))
        font = QtGui.QFont()
        font.setFamily("Sylfaen")
        font.setPointSize(14)
        self.login_edit.setFont(font)
        self.login_edit.setStyleSheet(
            "background-color: rgb(255, 255, 255);\n"
            "border-radius:10px;\n"
            "border: 1px solid;"
        )
        self.login_edit.setText("")
        self.login_edit.setMaxLength(35)
        self.login_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.login_edit.setObjectName("login_edit")

        # Параметры поля ввода пароля
        self.password_edit = QtWidgets.QLineEdit(self.widget)
        self.password_edit.setGeometry(QtCore.QRect(170, 160, 381, 41))
        font = QtGui.QFont()
        font.setFamily("Sylfaen")
        font.setPointSize(14)
        self.password_edit.setFont(font)
        self.password_edit.setStyleSheet(
            "background-color: rgb(255, 255, 255);\n"
            "border-radius: 10px;\n"
            "border: 1px solid"
        )
        self.password_edit.setText("")
        self.password_edit.setMaxLength(35)
        self.password_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.password_edit.setObjectName("password_edit")

        # Параметры чекбокса, отображающего введенный пароль
        self.check_box = QtWidgets.QCheckBox(self.widget)
        self.check_box.setGeometry(QtCore.QRect(236, 210, 220, 31))
        self.check_box.stateChanged.connect(self.show_hidden_password)
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(15)
        font.setBold(False)
        font.setWeight(50)
        self.check_box.setFont(font)
        self.check_box.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.check_box.setStyleSheet("")
        self.check_box.setObjectName("check_box")

        # Параметры поля отображения ошибок
        self.warnings = QtWidgets.QLabel(self.widget)
        self.warnings.setGeometry(QtCore.QRect(40, 320, 511, 31))
        font = QtGui.QFont()
        font.setFamily("Tw Cen MT Condensed Extra Bold")
        font.setPointSize(18)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.warnings.setFont(font)
        self.warnings.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.warnings.setStyleSheet("color: rgb(170, 0, 0);")
        self.warnings.setAlignment(QtCore.Qt.AlignCenter)
        self.warnings.setObjectName("warnings")
        login_form.setCentralWidget(self.widget)

        self.retranslate_ui(login_form)
        QtCore.QMetaObject.connectSlotsByName(login_form)

    def retranslate_ui(self, login_form):
        """Заполнение объектов текстом

        Args:
            login_form: объект графической формы
        """

        _translate = QtCore.QCoreApplication.translate
        login_form.setWindowTitle(_translate("login_form", "Войти"))
        self.title.setText(
            _translate("login_form", "Пожалуйста, авторизуйтесь на сервисе.")
        )
        self.login_label.setText(_translate("login_form", "Логин:"))
        self.password_label.setText(_translate("login_form", "Пароль:"))
        self.login_but.setText(_translate("login_form", "Авторизоваться"))
        self.login_edit.setPlaceholderText(
            _translate("login_form", "Пожалуйста, введите логин")
        )
        self.password_edit.setPlaceholderText(
            _translate("login_form", "Пожалуйста, введите пароль")
        )
        self.check_box.setText(_translate("login_form", "Показать пароль"))

    def show_hidden_password(self, __state):
        """Устанавливает режим отображения введенного пользователем пароля

        Args:
            __state (string): строка, указывающая на флаг чекбокса (checked/unchecked)
        """

        if __state == QtCore.Qt.Checked:
            self.password_edit.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.password_edit.setEchoMode(QtWidgets.QLineEdit.Password)
