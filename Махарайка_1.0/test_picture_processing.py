# sys - Используется для установки диреектории, из которой импортируются классы
import sys
# pytest - Используется для тестирования
import pytest
from picture_processing import EditIm
sys.path.append("../")

@pytest.mark.parametrize("image_name, result", [ ("maxon_testit.png", True),
                                        ("im_not_exist.png", "File not exist"),
                                        ("im_not_image.txt", "Файл не является изображением")])
def test_bw(image_name, result):
    assert EditIm().black_white_image(image_name) == result

@pytest.mark.parametrize("image_name, result", [ ("maxon_testit.png", True),
                                        ("im_not_exist.png", "Отсутствует файл изображения"),
                                        ("im_not_image.txt", "Файл не является изображением")])
def test_sepia(image_name, result):
    assert EditIm().sepia_image(image_name) == result

@pytest.mark.parametrize("image_name, result", [ ("maxon_testit.png", True),
                                        ("im_not_exist.png", "Отсутствует файл изображения"),
                                        ("im_not_image.txt", "Файл не является изображением")])
def test_invert(image_name, result):
    assert EditIm().invert_image(image_name) == result

@pytest.mark.parametrize("image_name, result", [ ("maxon_testit.png", True),
                                        ("im_not_exist.png", "Отсутствует файл изображения"),
                                        ("im_not_image.txt", "Файл не является изображением")])
def test_contrast(image_name, result):
    assert EditIm().contrast_image(image_name, 2) == result

@pytest.mark.parametrize("image_name, result", [ ("maxon_testit.png", True),
                                        ("im_not_exist.png", "Отсутствует файл изображения"),
                                        ("im_not_image.txt", "Файл не является изображением")])
def test_sharp(image_name, result):
    assert EditIm().sharpness_image(image_name, 2) == result
