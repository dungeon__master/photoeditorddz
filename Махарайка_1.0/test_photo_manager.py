# sys - Используется для установки диреектории, из которой импортируются классы
import sys
# sqlite3 - используется для работы с БД. Встроенная библиотека
import sqlite3
# pytest - Используется для тестирования
import pytest
from photo_manager import PhotoManager
import config
sys.path.append("../")


def test_clean_database():
    connect = sqlite3.connect(config.PATH_TO_DB)
    cursor = connect.cursor()
    request = "DELETE from picture"
    cursor.execute(request)
    connect.commit()
    connect.close()


ERROR = "Изображение с таким именем уже существует"


@pytest.mark.parametrize(
    "image_name, name_for_save, result",
    [
        ("test1", "testing", True),
        ("test1", "testing", ERROR),
        ("test2", "testing2", True),
    ],
)
def test_add_image(image_name, name_for_save, result):
    assert PhotoManager().add_image("admin", image_name, name_for_save, ".png") == result


ERROR = "Пожалуйста, выберите название для изображения"


@pytest.mark.parametrize(
    "image_name, result", [("testing", False), ("not_exist", True), ("", ERROR)]
)
def test_check_exist_image(image_name, result):
    assert PhotoManager().check_exist_image(image_name) == result


def test_update_image():
    assert PhotoManager().update_image("test1", ".png") is True


ERROR = "Пожалуйста, выберите изображение"


@pytest.mark.parametrize(
    "image_name, path, result",
    [("testing", "c:/python/test/img", True), ("", "c:/python/test/img", ERROR)],
)
def test_save_image(image_name, path, result):
    assert PhotoManager().save_image("admin", image_name, path) == result


@pytest.mark.parametrize("image_name, result", [("", ERROR), ("testing2", True)])
def test_delete_image(image_name, result):
    assert PhotoManager().delete_image("admin", image_name) == result
