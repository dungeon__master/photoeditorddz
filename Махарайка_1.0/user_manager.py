import sqlite3
import hashlib
import os.path

import config


class Database:
    """Взаимодействует с БД"""

    # Установка корректного пути к базу данных
    dbPath = os.path.join(config.BASE_DIR, config.PATH_TO_DB)

    def connect_database(self):
        """Подключение к БД"""
        self.connect = sqlite3.connect(self.dbPath)
        self.cursor = self.connect.cursor()

    def close(self):
        """Разрыв соединения с БД"""
        self.connect.close()


class UserManager(Database):
    """Взаимодействие с данными пользователя

    Args:
        Database (class): база данных
    """

    def get_user_data(self, __login):
        """Достает информацию о пользователе

        Args:
            __login (string): Имя пользователя

        Returns:
            [array]: Массив с userID, userName, password
        """

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Достаем из таблицы информацию о всех пользователях
        request = "SELECT userID, username, password FROM users WHERE username = ?"
        data = self.cursor.execute(request, (__login,)).fetchone()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return data

    def reg_user(self, __login, __password):
        """Создает нового пользователя

        Args:
            __login (string): Имя пользователя
            __password (string): Пароль
        """

        if __login == '' or __password == '':
            raise ValueError

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Получим хэш пароля
        password = self.pass_encr(__login, __password)

        # Заносим в таблицу users пользователя с хэшированным паролем
        request = "INSERT INTO users(username, password) VALUES (?, ?)"
        self.cursor.execute(request, (__login, password))
        self.connect.commit()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return True

    def check_validity(self, __login, __password):
        """Проверяет правильность введенных данных

        Args:
            __login (string): Имя пользователя
            __password (string): Пароль пользователя

        Returns:
            int: Флаг, указывающий на то, введены ли верные данные(0 - введённые данные верны, 1 - пользователя с таким именем не существует, 2 - пароль неверен)
        """

        # Достанем всю информацию о пользователе, логин которого введён
        user_data_arr = self.get_user_data(__login)
        password = self.pass_encr(__login, __password)

        # Проверим правильность введённых пользователем данных
        if (
            user_data_arr is not None
            and user_data_arr[1] == __login
            and user_data_arr[2] == password
        ):
            # Введенные данные верны
            return 0
        if user_data_arr is None:
            # Такого пользователя не существует
            return 1

        # В остальных случаях - пароль неверный
        return 2

    def get_user_id(self, __login):
        """Возвращает id пользователя по его имени

        Args:
            __login (string): Имя пользователя

        Returns:
            string: userID
        """

        # Проверка на "пустой" логин
        if __login == '':
            raise ValueError

        # Вызов из класса database метода connect_database
        self.connect_database()

        # Запрос, достающий userID из таблицы по имени пользователя
        request = "SELECT userID FROM users WHERE username = ?"
        id_arr = self.cursor.execute(request, (__login,)).fetchone()

        # Разрываем соединение с БД, close() - метод класса database
        self.close()
        return id_arr[0]

    def pass_encr(self, __user_name, __user_pass):
        """Шифрованиие пароля

        Аргументы:
        userName -- имя пользователя
        userPass -- пароль, который ввел пользователь
        """
        salt = hashlib.sha256(__user_name.encode()).hexdigest()

        # Хэширование пароля с солью и возвращение функцией результата
        hash_password = hashlib.sha256(__user_pass.encode() + salt.encode()).hexdigest()
        return hash_password
