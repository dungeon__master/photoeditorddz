from PyQt5 import QtCore, QtGui, QtWidgets


class UiSaveNameForm:
    """Представляет графическую форму, содержащую ввод названия сохраняемого изображения"""

    def setup_ui(self, save_name_form):
        """Формирует графическую форму

        Args:
            save_name_form (object): объект графической формы
        """
        save_name_form.setObjectName("save_name_form")
        save_name_form.setEnabled(True)
        save_name_form.resize(590, 265)
        save_name_form.setMinimumSize(QtCore.QSize(590, 265))
        save_name_form.setMaximumSize(QtCore.QSize(590, 362))
        font = QtGui.QFont()
        font.setPointSize(31)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        save_name_form.setFont(font)
        save_name_form.setMouseTracking(False)
        save_name_form.setLayoutDirection(QtCore.Qt.RightToLeft)
        save_name_form.setStyleSheet("background-color: rgb(85, 85, 127);")
        self.widget = QtWidgets.QWidget(save_name_form)
        self.widget.setStyleSheet("background-color: rgb(173, 173, 173);")
        self.widget.setObjectName("widget")
        self.title = QtWidgets.QLabel(self.widget)
        self.title.setGeometry(QtCore.QRect(0, 0, 600, 70))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(24)
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.title.setFont(font)
        self.title.setMouseTracking(True)
        self.title.setStyleSheet("")
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setWordWrap(False)
        self.title.setObjectName("title")
        self.save_new_name_but = QtWidgets.QPushButton(self.widget)
        self.save_new_name_but.setEnabled(True)
        self.save_new_name_but.setGeometry(QtCore.QRect(40, 160, 520, 52))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.save_new_name_but.setFont(font)
        self.save_new_name_but.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.save_new_name_but.setStyleSheet(
            "QPushButton{\n"
            "background-color: rgb(0, 170, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            "QPushButton:hover{\n"
            "background-color: rgb(0, 85, 0);\n"
            "color: rgb(255, 255, 255);\n"
            "border-radius: 11px;}\n"
            ""
        )
        self.save_new_name_but.setIconSize(QtCore.QSize(16, 16))
        self.save_new_name_but.setCheckable(False)
        self.save_new_name_but.setChecked(False)
        self.save_new_name_but.setObjectName("save_new_name_but")
        self.new_name_edit = QtWidgets.QLineEdit(self.widget)
        self.new_name_edit.setGeometry(QtCore.QRect(30, 90, 541, 41))
        font = QtGui.QFont()
        font.setFamily("Sylfaen")
        font.setPointSize(14)
        self.new_name_edit.setFont(font)
        self.new_name_edit.setStyleSheet(
            "background-color: rgb(255, 255, 255);\n"
            "border-radius:10px;\n"
            "border: 1px solid;"
        )
        self.new_name_edit.setText("")
        self.new_name_edit.setMaxLength(35)
        self.new_name_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.new_name_edit.setObjectName("new_name_edit")
        save_name_form.setCentralWidget(self.widget)

        self.retranslate_ui(save_name_form)
        QtCore.QMetaObject.connectSlotsByName(save_name_form)

    def retranslate_ui(self, save_name_form):
        """Заполнение объектов записями

        Args:
            save_name_form (object): объект графической формы
        """

        _translate = QtCore.QCoreApplication.translate
        save_name_form.setWindowTitle(
            _translate("save_name_form", "Enter the name of the image")
        )
        self.title.setText(_translate("save_name_form", "Введите название изображения"))
        self.save_new_name_but.setText(
            _translate("save_name_form", "Сохранить  изображение")
        )
        self.new_name_edit.setPlaceholderText(
            _translate("save_name_form", "Please enter image name")
        )
