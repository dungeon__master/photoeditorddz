import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import shutil

from login_form import UiLoginForm
from list import UiList
from photo_editing import UiEditForm
from user_manager import UserManager
from photo_manager import PhotoManager
import config
from picture_processing import EditIm
from save_changes_form import UiSaveNameForm
import os

# Логин пользователя
login = ""

# Отображение формы авторизации
app = QtWidgets.QApplication(sys.argv)
login_win = QtWidgets.QMainWindow()
login_form = UiLoginForm()
login_form.setup_ui(login_win)
login_win.show()


def event_close(__list_form):
    image = config.PATH_TO_IMAGES + __list_form.image.name + __list_form.image.extension
    image_path = os.path.join(config.BASE_DIR, image)
    os.remove(image_path)
    tmp_path = os.path.join(config.BASE_DIR, config.PATH_TO_TMP)
    try:
        shutil.rmtree(tmp_path)
    except:
        return False


# Вернуться из режима редактирования к списку фотографий
def return_to_list(__list_form):
    """Возвращение к окну выбора изображения

    Args:
        __list_form (object): объект графического интерфейса окна с таблицей
    """

    global login

    # Удаление изображения
    event_close(__list_form)
    __list_form.load_data(login)
    edit_win.close()
    list_win.show()


# Переход к окну редактирования сообщений
def open_edit_form(__list_form):
    """Открывает окно редактирования сообщений

    Args:
        __list_form (object): объект графического интерфейса окна с таблицей
    """

    global edit_win
    edit_win = QtWidgets.QMainWindow()
    edit_form = UiEditForm()
    edit_form.setup_ui(edit_win)
    list_win.close()
    edit_win.show()
    edit_form.show_image(__list_form.image)

    # Обработка нажатия кнопок фильтра
    edit_form.black_white_button.clicked.connect(
        lambda: make_black_white(__list_form, edit_form)
    )
    edit_form.sepia_button.clicked.connect(lambda: make_sepia(__list_form, edit_form))
    edit_form.negative_button.clicked.connect(lambda: make_negative(__list_form, edit_form))

    # Обработаем изменение показателя бегунка контраста
    edit_form.contrast_scroll_bar.valueChanged.connect(
        lambda: make_contrast(
            __list_form.image, edit_form.contrast_scroll_bar.value(), edit_form
        )
    )
    # Обработаем изменение показателя бегунка контраста
    edit_form.sharpness_scroll_bar.valueChanged.connect(
        lambda: make_sharpness(
            __list_form.image, edit_form.sharpness_scroll_bar.value(), edit_form
        )
    )

    # Возвращение на форму со списком изображений
    edit_form.back_to_list_button.clicked.connect(lambda: return_to_list(__list_form))
    edit_form.save_edits_button.clicked.connect(lambda: open_save_changes_form(__list_form))
    edit_form.cancel_edits_button.clicked.connect(
        lambda: update(login, __list_form, config.PATH_TO_IMAGES, edit_form)
    )
    app.aboutToQuit.connect(lambda: event_close(__list_form))


def update(__login, __list_form, __path, __edit_form):
    """Отменяет примененные фильтры

    Args:
        __login (string): имя пользователя
        __list_form (object): объект графической формы списка изображений
        __path (string): путь к изображению
        __edit_form (object): объект графической формы редактирования изображений
    """

    # Сохраняет в папку первичное изображение до применения по нему фильтров
    PhotoManager().save_image(__login, __list_form.image.name, __path)
    __edit_form.show_image(__list_form.image)


# Переход к окну со списком изображений
def open_list_form():
    """Переходит к окну со списком изображений"""

    global list_win
    global login
    list_win = QtWidgets.QMainWindow()

    # Отрисовка графического интерфейса таблицы с изображений
    list_form = UiList()
    list_form.setup_ui(list_win)
    login_win.close()
    list_form.load_data(login)
    list_win.show()

    # Обработка событий при нажатии на кнопки
    #
    # Переход к изменению изображению
    list_form.edit_record_button.clicked.connect(
        lambda: load_image_to_file(login, list_form, config.PATH_TO_IMAGES)
    )
    #
    # Загрузка изображения
    list_form.load_button.clicked.connect(lambda: add_image(login, list_form))
    #
    # Удаление изображения
    list_form.delete_record_button.clicked.connect(lambda: delete_image(login, list_form))
    #
    # Выгрузка изображения
    list_form.upload_button.clicked.connect(lambda: save_image(login, list_form))


def error_window(__error):
    """Окно с ошибкой

    Args:
        __error (string): текст ошибки
    """
    global msg

    # Формирование окна с ошибкой
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(__error)
    msg.setWindowTitle("Error")
    msg.show()


def open_save_changes_form(__list_form):
    """Открытие формы сохранения измений

    Args:
        __list_form (object): объект графической формы
    """

    global save_name_form

    # Создание графической формы сохранения изображения
    save_name_form = QtWidgets.QMainWindow()
    saving_form = UiSaveNameForm()
    saving_form.setup_ui(save_name_form)
    saving_form.new_name_edit.setText(__list_form.image.name)
    save_name_form.show()

    # Обработка нажатия на кнопку сохранения
    saving_form.save_new_name_but.clicked.connect(
        lambda: save_changed_image(save_name_form, saving_form, __list_form)
    )


def save_changed_image(__saving_win, __saving_form, __list_form):
    """Осуществляется сохранение измененного изображения в БД

    Args:
        __saving_win (object): окно сохранения изображений
        __saving_form (object): графическая форма сохранения изображений
        __list_form (object): графическая форма со списком изображений
    """

    global login

    # Получение информации об изображении
    image_name = __list_form.image.name
    extension = __list_form.image.extension
    name = __saving_form.new_name_edit.text()
    flag = PhotoManager().check_exist_image(name)

    # Загрузка изображения в БД. Если не существует - загружается новое, если существует - обновляется старое
    if flag is True:
        PhotoManager().add_image(login, image_name, name, extension)
        __saving_win.close()
        return_to_list(__list_form)
    elif flag is False:
        PhotoManager().update_image(name, extension)
        __saving_win.close()
        return_to_list(__list_form)

    # Вывод ошибки
    else:
        error_window(flag)


def add_image(__login, __list_form):
    """Добавление изображения в БД

    Args:
        __login (string): имя пользователя
        __list_form (object): объект графической формы
    """

    error = PhotoManager().add_image(__login)
    __list_form.load_data(__login)
    if error is not True:
        error_window(error)


def save_image(__login, __list_form):
    """Выгрузка изображения из БД

    Args:
        __login (string): имя пользователя
        __list_form (object): объект графической формы
    """
    image_name = __list_form.image.name
    error = PhotoManager().save_image(__login, image_name)
    __list_form.load_data(__login)
    if error is not True:
        error_window(error)


def delete_image(__login, __list_form):
    """Удаление изображения из БД

    Args:
        __login (string): имя пользователя
        __list_form (object): объект графической формы
    """
    error = PhotoManager().delete_image(__login, __list_form.image.name)
    __list_form.load_data(__login)
    if error is not True:
        error_window(error)


def load_image_to_file(__login, __list_form, __path):
    """Загрузка изображения в папку img

    Args:
        __login (string): имя пользователя
        __list_form (ui_list): объект графической формы
        __path (string): путь к изображению
    """
    image_name = __list_form.image.name
    error = PhotoManager().save_image(__login, image_name, __path)
    if error is not True:
        open_list_form()
        error_window(error)
    else:
        open_edit_form(__list_form)


def make_black_white(__list_form, __edit_form):
    """Переводит изображение в ЧБ

    Args:
        __list_form (object): объект графической формы списка изображений
        __edit_form (object): объект графической формы изменения изображений
    """
    picture_full_name = __list_form.image.name + __list_form.image.extension
    EditIm().black_white_image(picture_full_name)
    __edit_form.show_image(__list_form.image)


def make_sepia(__list_form, __edit_form):
    """Переводит изображение в сепию

    Args:
        __list_form (object): объект графической формы списка изображений
        __edit_form (object): объект графической формы изменения изображений
    """

    picture_full_name = __list_form.image.name + __list_form.image.extension
    EditIm().sepia_image(picture_full_name)
    __edit_form.show_image(__list_form.image)


def make_negative(__list_form, __edit_form):
    """Переводит изображение в негатив

    Args:
        __list_form (object): объект графической формы списка изображений
        __edit_form (object): объект графической формы изменения изображений
    """

    picture_full_name = __list_form.image.name + __list_form.image.extension
    EditIm().invert_image(picture_full_name)
    __edit_form.show_image(__list_form.image)


def make_contrast(__image, coef, __edit_form):
    """Применяет к изображению показатель бегунка контраста

    Args:
        __image (string): объект изображения
        coef (int): степень контраста (по умолчаю - 1, менее 1 - уменьшенный контраст, более 1 - увеличенный контраст)
    """

    picture_full_name = __image.name + __image.extension
    EditIm().contrast_image(picture_full_name, coef)
    __edit_form.show_image(__image)


def make_sharpness(__image, coef, __edit_form):
    """Применяет к изображению показатель бегунка резкости

    Args:
        __image (string): объект изображения
        coef (int): степень резкости (по умолчаю - 1, менее 1 - уменьшенная резкость, более 1 - увеличенная резкость)
    """

    picture_full_name = __image.name + __image.extension
    EditIm().sharpness_image(picture_full_name, coef)
    __edit_form.show_image(__image)


def auth():
    """Аутентификация пользователя"""
    global login

    # Получение данных из полей
    password = login_form.password_edit.text()
    login = login_form.login_edit.text()

    # Вывод ошибки при неудачной аутентификации
    if login == "" or password == "":
        login_form.warnings.setText("Введёны неверные данные!!!")
        return False
    flag = UserManager().check_validity(login, password)
    if flag == 2:
        login_form.warnings.setText("Введёны неверные данные!!!")
        return False

    # Открытие окна со списком фотографий при удачной аутентификации
    if flag == 0:
        open_list_form()
        return True
    if flag == 1:
        UserManager().reg_user(login, password)
        open_list_form()
        return True


# Аутентификация пользователя при нажатии на кнопку
login_form.login_but.clicked.connect(auth)

sys.exit(app.exec_())
