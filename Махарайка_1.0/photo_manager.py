# sqlite3 - используется для работы с БД. Встроенная библиотека
import sqlite3
# os - Используется для определения пути для сохрения или загрузки из-я
import os
# datetime - Используется для определения времени создания и редактирования из-я
from datetime import datetime
# pytz - Используетс для установки часового пояса
import pytz
from user_manager import UserManager
from list import UiList
import config


class Database:
    """Взаимодействие с БД"""

    def connect_database(self):
        """Устанавливает соединение с БД"""

        self.connect = sqlite3.connect(config.PATH_TO_DB)
        self.cursor = self.connect.cursor()

    def close(self):
        """Разрывает соединение с БД"""

        self.connect.close()


class PhotoManager(Database):
    """Взаимодействует с изображениями

    Args:
        Database (class): база данных
    """

    def import_pict_binary(self, __image_path):
        """Переводит изображения в BLOB

        Args:
            __image_path (string): путь к изображению

        Returns:
            BLOB: изображение в формате BLOB
        """

        # Взаимодействуем с изображением побайтно
        with open(__image_path, "rb") as file:
            pict_binary = file.read()

        # Закрываем поток
        file.close()

        # Возвращение бинарника изображения
        return pict_binary

    def write_to_file(self, data, __file_path):
        """Запись из БД по заданному пути

        Args:
            data (BLOB): бинарник изображения
            __file_path (string): путь для записи изображения
        """

        with open(__file_path, "wb") as file:
            file.write(data)

    # Перевод размера файла в необходимый формат
    def get_size(self, __size):
        """Получает размер изображения

        Args:
            __size (int): Размер в байтах

        Returns:
            str: Строка с информацией о размере изображения
        """

        # Рассчет размера изображение и вывод в его оптимальной мере размера
        # Изначально - размер файла в байтах
        fullsize = str(__size) + "Bytes"

        # Если размер > 2^10, то повысим степень и переведем изображение в килобайты
        if __size > 1024:
            __size = round(__size / 1024 * 10) / 10
            fullsize = str(__size) + "Kb"

        # Если все ещё размер > 2^10, то повысим степень и переведем изображение в мегабайты
        if __size > 1024:
            __size = round(__size / 1024 * 10) / 10
            fullsize = str(__size) + "Mb"
        return fullsize

    def save_image(self, __login, __image_name, __path=""):
        """Сохраняет изображение по пути

        Args:
            __login (string): имя пользователя
            __image_name (string): Название изображения
            __path (str, optional): путь к месту сохранения изображения. по умолчанию ''.

        Returns:
            mixed: true - ошибок нет, иначе - ошибка
        """

        # Подключение к БД
        self.connect_database()

        # Выход из функции, в случае если изображение не выбрано
        if __image_name is None or __image_name == "":
            self.close()
            return "Пожалуйста, выберите изображение"

        # Формирование запроса
        uid = UserManager().get_user_id(__login)
        request_to_picture = "SELECT image, extension FROM picture WHERE name = ? and userId = ?"
        image, extension = self.cursor.execute(
            request_to_picture, (__image_name, uid)
        ).fetchone()

        # Определение пути для сохранения изображения
        if __path == "":
            # Если не передан - получаем пользовательский
            list_form = UiList()
            image_path = list_form.get_dir() + "/" + __image_name + extension
        else:
            # Если передан - сохраняем по пути
            image_path = __path + "/" + __image_name + extension

        # Запись изображения в файловую систему
        self.write_to_file(image, image_path)
        return True

    def delete_image(self, __login, __image_name):
        """Удаление изображения из БД

        Args:
            __login (string): имя пользователя
            __image_name (string): название изображения (без расширения)

        Returns:
            mixed: true - ошибок нет, иначе - ошибка
        """

        # Формирование запроса
        self.connect_database()

        # Обработка ошибок
        if __image_name is None or __image_name == "":
            self.close()
            return "Пожалуйста, выберите изображение"

        # Получаем id пользователя
        uid = UserManager().get_user_id(__login)

        # Запрос на удаление
        request = "DELETE from picture where name = ? and userID = ?"
        order = (__image_name, uid)
        self.cursor.execute(request, order)
        self.connect.commit()

        # Разрыв соединения
        self.close()
        return True

    def add_image(self, __login, __true_name="", __name="", __extension=""):
        """Добавляет изображение в БД

        Args:
            __login (string): имя пользователя
            __true_name (string, optional): действительное имя изображения
                                            по умолч. ''
            __name (string, optional): имя изображения, под которым хочет сохранить его пользователь
                                       по умолч. ''
            __extension (string, optional): расширение изображения
                                           по умолч. ''

        Returns:
            mixed: обработка ошибок
        """

        # Установка соединения
        self.connect_database()

        if __name == "":
            # Если параметры функции не заданы, то спрашиваем путь до файла у пользователя
            list_form = UiList()
            image_path = list_form.get_file_name()

            file_name, extension = os.path.splitext(image_path)
            name = file_name.split("/")[-1]
        else:
            # Сохранение изображения в БД
            image = config.PATH_TO_IMAGES + __true_name + __extension
            image_path = os.path.join(config.BASE_DIR, image)
            name = __name
            extension = __extension

        # Если пользователь нажал Отмена или Закрыть окно
        if image_path is None or image_path == "":
            self.close()
            return True

        # Перевод изображения в BLOB
        image = self.import_pict_binary(image_path)

        # Получение размера файла
        size = self.get_size(os.path.getsize(image_path))

        # Опеределяем время занесения в БД
        time_zone = pytz.timezone("Europe/Moscow")
        date = datetime.now(time_zone)

        # Обрезание милисекунд
        time = str(date).split(".", maxsplit=1)[0]

        # Достаем id пользователя из БД
        uid = UserManager().get_user_id(__login)

        # Формирование запроса
        request = "INSERT INTO picture(name, extension, createdAt, changedAt, size, image, userID) VALUES (?, ?, ?, ?, ?, ?, ?)"
        order = (name, extension, time, "Not changed", size, image, uid)

        # Пытаемся выполнить запрос
        try:
            self.cursor.execute(request, order)
            self.connect.commit()
            self.close()

        # Если изображение уже существует, то возвращаем ошибку
        except sqlite3.Error:
            self.close()
            return "Изображение с таким именем уже существует"

        return True

    def update_image(self, __name, __extension):
        """Занесение измененного изображения в БД

        Args:
            __name (string): имя изображения
            __extension (string): расширение изображения
        """

        # Установление подключения к БД
        self.connect_database()

        # Путь к изображению
        image = config.PATH_TO_IMAGES + __name + __extension
        image_path = os.path.join(config.BASE_DIR, image)

        # Возвращает бинарник изображения
        image = self.import_pict_binary(image_path)

        # Расчет размера изображения
        size = self.get_size(os.path.getsize(image_path))

        # Установление времени изменения изображения
        timezone = pytz.timezone("Europe/Moscow")
        date = datetime.now(timezone)

        # Убираем милисекунды
        time = str(date).split(".", maxsplit=1)[0]

        # Формирование запроса
        request = "UPDATE picture set changedAt = ?, size = ?, image = ? where name = ?"
        order = (time, size, image, __name)
        self.cursor.execute(request, order)
        self.connect.commit()
        self.close()
        return True

    def check_exist_image(self, __image_name):
        """Проверяет, есть ли изображение с таким именем в БД

        Args:
            __image_name (string): имя изображения

        Returns:
            mixed: обработка ошибок
        """

        # Устанавливает соединение с БД
        self.connect_database()

        # Если имя изображение не указано - выдаем ошибку
        if __image_name == "":
            self.close()
            return "Пожалуйста, выберите название для изображения"

        # Формирование запроса
        request = "SELECT pictureId from picture where name = ?"
        order = (__image_name,)
        picture_id = self.cursor.execute(request, order).fetchone()
        self.close()

        # Если картинки нет - true, если есть - false
        if picture_id is None:
            return True
        return False
